// TODO : contact-app

// * required modules

const yargs = require('yargs');
const { saveContacts, listContact, detailContact, deleteContact } = require('./contact');

// * add contact feature

yargs
	.command({
		command: 'add',
		describe: 'Add new contact',
		builder: {
			name: {
				describe: 'Full name',
				demandOption: true,
				type: 'string',
			},
			email: {
				describe: 'Email',
				demandOption: false,
				type: 'string',
			},
			mobilePhone: {
				describe: 'Mobile Phone',
				demandOption: true,
				type: 'string',
			},
		},
		handler(argv) {
			saveContacts(argv.name, argv.email, argv.mobilePhone);
		},
	})
	.demandCommand();

// * show contact list feature

yargs.command({
	command: 'list',
	describe: 'show contact list',
	handler() {
		listContact();
	},
});

// * contact detail feature

yargs.command({
	command: 'detail',
	describe: 'show contact detail by name',
	builder: {
		name: {
			describe: 'Full name',
			demandOption: true,
			type: 'string',
		},
	},
	handler(argv) {
		detailContact(argv.name);
	},
});

// * Delete contact feature

yargs.command({
	command: 'delete',
	describe: 'delete contact by name',
	builder: {
		name: {
			describe: 'Full name',
			demandOption: true,
			type: 'string',
		},
	},
	handler(argv) {
		deleteContact(argv.name);
	},
});

yargs.parse();

// * required  node_moduleS

const fs = require('fs');
const chalk = require('chalk');
const validator = require('validator');

// * create directory path data if that's not exist

const dirPath = './data';
if (!fs.existsSync(dirPath)) {
	fs.mkdirSync(dirPath);
}

// * create file contacts.json if that's not exist

const dataPath = './data/contacts.json';
if (!fs.existsSync(dataPath)) {
	fs.writeFileSync(dataPath, '[]', 'utf-8');
}

// * create a function for question section in the form of a promise

const writeQuestions = (question) => {
	return new Promise((resolve, rejects) => {
		rl.question(question, (data) => {
			resolve(data);
		});
	});
};

// * create a function for load contact

const loadContact = () => {
	const file = fs.readFileSync('data/contacts.json', 'utf-8');
	const contacts = JSON.parse(file);

	return contacts;
};

// * create a function for save contacts

const saveContacts = (name, email, mobilePhone) => {
	const contact = {
		name,
		email,
		mobilePhone,
	};

	// * load contacts

	const contacts = loadContact();

	// * check if any contacts are duplicate

	const duplicate = contacts.find((contact) => contact.name === name);

	if (duplicate) {
		console.log(chalk.bgRed.bold('Duplicate contact name, Please use another name !'));

		return false;
	}

	// * check email format

	if (email) {
		if (!validator.isEmail(email)) {
			console.log(chalk.bgRed.bold('Email invalid !'));

			return false;
		}
	}

	// * check mobile phone format

	if (!validator.isMobilePhone(mobilePhone, 'id-ID')) {
		console.log(chalk.bgRed.bold('mobile Phone invalid !'));

		return false;
	}

	contacts.push(contact);

	fs.writeFileSync('data/contacts.json', JSON.stringify(contacts));
	console.log(chalk.bgGreen.bold('Thank You for your attention !'));
};

// * create a function for contact list

const listContact = () => {
	const contacts = loadContact();

	console.log(chalk.cyan.inverse.bold('List Contact : '));
	contacts.forEach((val, i) => {
		console.log(`${i + 1}. ${val.name} - ${val.mobilePhone}`);
	});
};

// * create a function for contact detail

const detailContact = (name) => {
	const contacts = loadContact();
	const contact = contacts.find((contact) => contact.name.toLowerCase() === name.toLowerCase());

	if (!contact) {
		console.log(chalk.bgRed.bold(`${name} not found !`));

		return false;
	}

	console.log(chalk.cyan.inverse.bold(contact.name));
	console.log(contact.mobilePhone);

	if (contact.email) {
		console.log(contact.email);
	}
};

// * create a function for delete contact

const deleteContact = (name) => {
	const contacts = loadContact();
	const newContacts = contacts.filter((contacts) => contacts.name.toLowerCase() !== name.toLowerCase());

	if (contacts.length === newContacts.length) {
		console.log(chalk.bgRed.bold(`${name} not found !`));

		return false;
	}

	fs.writeFileSync('data/contacts.json', JSON.stringify(newContacts));
	console.log(chalk.bgGreen.bold(`The contact named ${name} successfully deleted !`));
};

module.exports = { saveContacts, listContact, detailContact, deleteContact };
